<?php require_once('app/init.php'); ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>OshSU Times</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Scada&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="/styles/vendor.min.css?v=<?=filemtime(SERVER_ROOT . '/styles/vendor.min.css')?>">
    <link rel="stylesheet" href="/styles/app.min.css?v=<?=filemtime(SERVER_ROOT . '/styles/app.min.css')?>">
  </head>
  <body ng-app="oshsutimesApp">
    <!--[if lte IE 8]>
      <p class="browsehappy">Сиздин браузер бул сайтты иштете албайт. Сураныч, <a href="http://browsehappy.com/">жаңы браузер</a> алыңыз!</p>
    <![endif]-->

    <div ui-view></div>

    <script src="/scripts/vendor.min.js?v=<?=filemtime(SERVER_ROOT . '/scripts/vendor.min.js')?>"></script>
    <script src="/scripts/app.min.js?v=<?=filemtime(SERVER_ROOT . '/scripts/app.min.js')?>"></script>
    <script src="/scripts/controllers.min.js?v=<?=filemtime(SERVER_ROOT . '/scripts/controllers.min.js')?>"></script>
    <script src="/scripts/services.min.js?v=<?=filemtime(SERVER_ROOT . '/scripts/services.min.js')?>"></script>
</body>
</html>
