<?php
class DbGroup {
	private $id = 0;
	private $title = '';
	private $faculty = 0;
	private $starosta = '{}';

	public function getId(){
		return $this->id;
	}

	public function getTitle(){
		return $this->title;
	}

	public function getFaculty(){
		return $this->faculty;
	}

	public function getStarosta(){
		return json_decode($this->starosta);
	}

	public function setId($id){
		$this->id = _intval($id);
	}

	public function setTitle($title){
		$this->title = $title;
	}

	public function setFaculty($faculty){
		$this->faculty = _intval($faculty);
	}

	public function setStarosta($starosta){
		$this->starosta = is_array($starosta) ? json_encode($starosta) : $starosta;
	}
}