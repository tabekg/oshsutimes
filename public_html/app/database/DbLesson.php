<?php
class DbLesson {
	private $id = 0;
	private $tapya = 0;
	private $title = '';
	private $rooms = '[]';
	private $building = '';
	private $time_start = 0;
	private $time_end = 0;
	private $day = 0;

	public function getId(){
		return $this->id;
	}

	public function getGroup(){
		return $this->taypa;
	}

	public function getTitle(){
		return $this->title;
	}

	public function getRooms(){
		return json_decode($this->rooms);
	}

	public function getBuilding(){
		return $this->building;
	}

	public function getTimeStart(){
		return $this->time_start;
	}

	public function getTimeEnd(){
		return $this->time_end;
	}

	public function getDay(){
		return $this->day;
	}
}