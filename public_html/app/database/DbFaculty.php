<?php
class DbFaculty {
	private $id = 0;
	private $name = '';

	public function getId(){
		return $this->id;
	}

	public function getName(){
		return $this->name;
	}

	public function setId($id){
		$this->id = _intval($id);
	}

	public function setName($name){
		$this->name = $name;
	}
}