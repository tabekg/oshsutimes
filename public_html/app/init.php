<?php
date_default_timezone_set('Asia/Bishkek');

if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '192.168.43.1')
  define('LOCALHOST', true);
else
  define('LOCALHOST', false);

if (LOCALHOST) {
  //ini_set('display_errors', 1);
  //ini_set('display_startup_errors', 1);
  //error_reporting(E_ALL);

  define('SITE_DOMAIN', $_SERVER['HTTP_HOST']);
  define('SITE_PROTOCOL', 'http://');
  define('DB_USER', 'root');
  define('DB_PASSWORD', '');
  define('DB_NAME', 'oshsu_times');
} else {
  define('SITE_DOMAIN', 'time.oshsu.kg');
  define('SITE_PROTOCOL', 'http://');
  define('DB_USER', 'admin_time');
  define('DB_PASSWORD', '');
  define('DB_NAME', 'admin_time');
}

// SERVER
define('SERVER_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SERVER_BACKEND', SERVER_ROOT . '/app');
define("SERVER_TIME", time());

// DIR
define('DIR_CONTROLLERS', SERVER_BACKEND . '/controllers');
define('DIR_CORE', SERVER_BACKEND . '/core');
define('DIR_DATABASE', SERVER_BACKEND . '/database');

// DEFAULT
define("DEFAULT_CHARSET", "utf-8");

// SITE
define("SITE_URL", SITE_PROTOCOL . SITE_DOMAIN);

// BRAND
define("BRAND_NAME", "OshSU Times");
define("BRAND_DESCRIPTION", "OshSU Times");
define('BRAND_ICON', "/images/ico.jpg");
define('BRAND_COLOR', "#7fccf7");

// DB
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_PREFIX', 'tabekg');

require_once('core/functions.php');

spl_autoload_register(function ($class_name) {
    $path = 0;
    if (_substr($class_name, 0, 2) == "Db" && $class_name != 'Db') $path = DIR_DATABASE . '/' . $class_name . '.php';
    elseif (_substr($class_name, _strlen($class_name) - 10, 10) == 'Controller' && $class_name != 'Controller') $path = DIR_CONTROLLERS . "/" . $class_name . ".php";
    else $path = DIR_CORE . '/' . $class_name . '.php';
    require_once $path;
});