<?php
class App {
	protected $controller = 'default';
	protected $method = 'index';
	protected $params = [];

	public function __construct($controller, $method, $db){
		$path = DIR_CONTROLLERS . '/' . strtoupper(substr($controller, 0, 1)) . substr($controller, 1, strlen($controller) - 1) . 'Controller.php';

		if (file_exists($path)){
			$this->controller = $controller;
			unset($controller);
		} else {
			$path = DIR_CONTROLLERS . '/' . strtoupper(substr($this->controller, 0, 1)) . substr($this->controller, 1, strlen($this->controller) - 1) . 'Controller.php';
		}

		require_once($path);

		$this->controller = strtoupper(substr($this->controller, 0, 1)) . substr($this->controller, 1, strlen($this->controller) - 1) . 'Controller';
		$this->controller = new $this->controller($db);

		if (isset($method)){
			if (method_exists($this->controller, $method)){
				$this->method = $method;
				unset($method);
			}
		}

		unset($_POST['secret']);
		unset($_POST['controller']);
		unset($_POST['method']);

		$this->params = isset($_POST['params']) ? $_POST['params'] : [];

		call_user_func_array([$this->controller, $this->method], $this->params);
	}
}