<?php
class Db {
  public $dbId = false;
  public $queryCount = 0;
  public $queryList = array();
  public $queryId = false;
  public $timeTaken = 0;

  public function __construct($host, $user, $password, $name){
    if ($this->dbId == false){
      $this->dbId = mysqli_connect($host, $user, $password, $name);
      if ($this->dbId->connect_errno) die("Error in Database connection!");
      mysqli_set_charset($this->dbId, DB_CHARSET);
    }
  }

  public function disconnect(){
    if ($this->dbId){
      @mysqli_close($this->dbId);
      $this->dbId = false;
    }
    return true;
  }

  public function getRealTime(){
    list($seconds, $microseconds) = explode(" ", microtime());
    return ((float) $seconds + (float) $microseconds);
  }

  public function safe($source){
    if ($this->dbId) return mysqli_real_escape_string($this->dbId, $source);
    else return mysql_real_escape_string($source);
  }

  public function free($query = false){
    if ($query == false) $query = $this->queryId;
    @mysqli_free_result($query);
  }

  public function insertId(){
    return mysqli_insert_id($this->dbId);
  }

  public function numRows($query = false){
    if ($query == false) $query = $this->queryId;
    return mysqli_num_rows($query);
  }

  public function getRow($query = false){
    if ($query == false) $query = $this->queryId;
    return mysqli_fetch_assoc($query);
  }

  public function getArray($query = false){
    if ($query == false) $query = $this->queryId;
    return mysqli_fetch_array($query);
  }

  public function query($sql){
    $before = $this->getRealTime();

    if (!$this->dbId) $this->connect();

    $sql = str_replace("PREFIX", DB_PREFIX, $sql);

    if (!($this->queryId = mysqli_query($this->dbId, $sql))){
      echo "Error in Database query! => " . $sql;
      exit();
    }

    $timeTaken = $this->getRealTime() - $before;
    $this->timeTaken += $timeTaken;
    $this->queryCount++;
    array_push($this->queryList, array( "sql" => $sql, "timeTaken" => $timeTaken ));

    return $this->queryId;
  }
}
