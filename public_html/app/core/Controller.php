<?php
class Controller {
	public $db = null;

	public function __construct($db){
		$this->db = $db;
	}

	public function response($data = [], $code = 200, $type = 'application/json'){
		http_response_code($code);
		header('Content-Type: ' . $type . '; charset=utf-8');
		if ($data !== null) echo json_encode($data);
	}
}