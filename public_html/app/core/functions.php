<?php
function _strlen($text){
  return iconv_strlen($text, DEFAULT_CHARSET);
}

function _substr($text, $start, $end){
  return iconv_substr($text, $start, $end, DEFAULT_CHARSET);
}

function _empty($value){
  return $value == "" || $value == null || empty($value);
}

function _hash($letter = 0, $data = ''){
  $hash = md5('TABEKG' . 'OSHSU' . $data . 'BEKA');
  if ($letter < 1) $letter = _strlen($hash) - 1;
  return _substr($hash, 0, $letter);
}

function _redir($to){
  if (_empty($to)) $to = "/";
  header("Location: " . $to);
  exit();
}

function _intval($value){
  return intval($value);
}

function _input($source){
  return $source;
}

function _week($week, $type = 'lower|full'){
  $exp = explode('|', $type);
  $type = array('lower', 'full');
  if ($exp[0] == 'upper' || $exp[0] == 'firstUpper') $type[0] = $exp[0];
  if ($exp[1] == 'short') $type[1] = $exp[1];

  $return = 'неизвестно';
  switch(_intval($month)){
    case 1: $return = 'понедельник'; break;
    case 2: $return = 'вторник'; break;
    case 3: $return = 'среда'; break;
    case 4: $return = 'четверг'; break;
    case 5: $return = 'пятница'; break;
    case 6: $return = 'суббота'; break;
    case 7: $return = 'воскресенье'; break;
  }

  switch($type[0]){
    case 'upper':
      $return = strtoupper($return);
      break;
    case 'firstUpper':
      $return = strtoupper(_substr($return, 0, 1)) . _substr($return, 1, _strlen($return - 1));
      break;
  }

  if ($type[1] == 'short') $return = _substr($return, 0, 3);

  return $return;
}

function _month($month, $type = 'lower|full'){
  $exp = explode('|', $type);
  $type = array('lower', 'full');
  if ($exp[0] == 'upper' || $exp[0] == 'firstUpper') $type[0] = $exp[0];
  if ($exp[1] == 'short') $type[1] = $exp[1];

  $return = 'неизвестно';
  switch(_intval($month)){
    case 1: $return = 'январь'; break;
    case 2: $return = 'февраль'; break;
    case 3: $return = 'март'; break;
    case 4: $return = 'апрель'; break;
    case 5: $return = 'май'; break;
    case 6: $return = 'июнь'; break;
    case 7: $return = 'июль'; break;
    case 8: $return = 'август'; break;
    case 9: $return = 'сентябрь'; break;
    case 10: $return = 'октябрь'; break;
    case 11: $return = 'ноябрь'; break;
    case 12: $return = 'декабрь'; break;
  }

  switch($type[0]){
    case 'upper':
      $return = strtoupper($return);
      break;
    case 'firstUpper':
      $return = strtoupper(_substr($return, 0, 1)) . _substr($return, 1, _strlen($return - 1));
      break;
  }

  if ($type[1] == 'short') $return = _substr($return, 0, 3);

  return $return;
}

function _safe($source){
  return htmlentities($source, ENT_QUOTES, DEFAULT_CHARSET);
}
