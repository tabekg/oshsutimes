<?php
class FacultyController extends Controller {
	public function index(){
		$this->response(null, 400);
	}

	public function get(){

	}

	public function getAll(){
		$list = $this->db->query('SELECT * FROM `PREFIX_faculties` ORDER BY `id` ASC');
		$response = [];
		while($row = $list->fetch_object('DbFaculty')){
			array_push($response, $this->parser($row));
		}
		return $this->response($response, 200);
	}

	private function parser($row){
		return [
			'id' => $row->getId(),
			'name' => $row->getName()
		];
	}
}