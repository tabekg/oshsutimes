<?php
class GroupController extends Controller {
	public function index(){
		$this->response(null, 400);
	}

	public function get($group){

	}

	public function getAll(){
		$list = $this->db->query('SELECT * FROM `PREFIX_groups` ORDER BY `id` ASC');
		$response = [];
		while($row = $list->fetch_object('DbGroup')){
			array_push($response, $this->parser($row));
		}
		return $this->response($response, 200);
	}

	public function getByFaculty($faculty){
		$db = $this->db;
		$list = $db->query('SELECT * FROM `PREFIX_groups` WHERE `faculty`=' . $db->safe($faculty) . ' ORDER BY `id` ASC');
		$response = [];
		while($row = $list->fetch_object('DbGroup')){
			array_push($response, $this->parser($row));
		}
		return $this->response($response, 200);
	}

	private function parser($row){
		return [
			'id' => $row->getId(),
			'title' => $row->getTitle(),
			'faculty' => $row->getFaculty(),
			'starosta' => $row->getStarosta()
		];
	}
}