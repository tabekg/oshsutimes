<?php
class AdminController extends Controller {
	private function inspectPassword($password){
		$db = $this->db;
		$query = $db->query("SELECT * FROM `PREFIX_bi` WHERE `name`='hash' and `value`='" . $db->safe($password) . "' ORDER BY `id` DESC LIMIT 1");
		if ($db->numRows($query) > 0) return true;
		else return false;
	}

	public function login($password){
		$password = md5("{$password}tabekg");
		$p = $this->inspectPassword($password);
		if ($p) {
			$this->response([
				'result' => true,
				'hash' => $password
			], 200);
		} else {
			$this->response([
				'result' => false,
				'hash' => ''
			], 200);
		}
	}

	public function getStats($password){
		if ($this->inspectPassword($password)){
			$db = $this->db;
			$response = [
				'lessons' => $db->query('SELECT COUNT(*) AS `c` FROM `PREFIX_lessons` WHERE `title`!=""')->fetch_object()->c,
				'breath' => $db->query('SELECT COUNT(*) AS `c` FROM `PREFIX_lessons` WHERE `title`=""')->fetch_object()->c,
				'faculties' => $db->query('SELECT COUNT(*) AS `c` FROM `PREFIX_faculties`')->fetch_object()->c,
				'groups' => $db->query('SELECT COUNT(*) AS `c` FROM `PREFIX_groups`')->fetch_object()->c
			];
			$this->response($response, 200);
		} else $this->response([], 403);
	}

	public function addFaculty($password, $name){
		if ($this->inspectPassword($password)){
			$db = $this->db;
			$status = 'error';
			if ($db->query('SELECT COUNT(*) AS `c` FROM `PREFIX_faculties` WHERE `name`="' . $db->safe($name) . '"')->fetch_object()->c > 0) $status = 'exists';
			else $status = $db->query('INSERT INTO `PREFIX_faculties` SET `name`="' . $db->safe($name) . '"') ? 'success' : 'error';
			$this->response([
				'status' => $status
			], 200);
		} else $this->response([], 403);
	}

	public function addGroup($password, $name, $starosta, $faculty){
		if ($this->inspectPassword($password)){
			$db = $this->db;
			$status = 'error';
			$starosta = json_encode($starosta);
			if ($db->query('SELECT COUNT(*) AS `c` FROM `PREFIX_groups` WHERE `title`="' . $db->safe($name) . '" and `faculty`=' . $db->safe($faculty))->fetch_object()->c > 0) $status = 'exists';
			else $status = $db->query('INSERT INTO `PREFIX_groups` SET `title`="' . $db->safe($name) . '", `faculty`=' . $db->safe($faculty) . ', `starosta`="' . $db->safe($starosta) . '"') ? 'success' : 'error';
			$this->response([
				'status' => $status
			], 200);
		} else $this->response([], 403);
	}

	public function addLesson($password, $name, $day, $time, $building, $rooms, $position, $group){
		if ($this->inspectPassword($password)){
			$db = $this->db;
			$status = 'error';
			$time['start'] = explode(':', $time['start']);
			$time['end'] = explode(':', $time['end']);
			$time['start'] = ($time['start'][0] * 3600) + ($time['start'][1] * 60);
			$time['end'] = ($time['end'][0] * 3600) + ($time['end'][1] * 60);
			$status = $db->query('INSERT INTO `PREFIX_lessons` SET
				`taypa`=' . $db->safe($group) . ',
				`title`="' . $db->safe($name) . '",
				`rooms`="' . $db->safe(json_encode(explode(', ', $rooms))) . '",
				`building`="' . $db->safe($building) . '",
				`time_start`=' . $db->safe($time['start']) . ',
				`time_end`=' . $db->safe($time['end']) . ',
				`day`=' . $db->safe($day) . ',
				`position`=' . $db->safe($position) . '
			') ? 'success' : 'error';
			$this->response([
				'status' => $status
			], 200);
		} else $this->response([], 403);
	}
}