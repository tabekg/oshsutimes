<?php
class LessonController extends Controller {
	public function index(){
		$this->response(null, 400);
	}

	public function get($group){

	}

	public function getAll(){
		$list = $this->db->query('SELECT * FROM `PREFIX_lessons` ORDER BY `position`, `id` ASC');
		$response = [
			0 => [],
			1 => [],
			2 => [],
			3 => [],
			4 => [],
			5 => []
		];
		while($row = $list->fetch_object('DbLesson')){
			array_push($response[$row->getDay() - 1], $this->parser($row));
		}
		return $this->response($response, 200);
	}

	public function getByGroup($group, $forApp){
		$list = $this->db->query('SELECT * FROM `PREFIX_lessons` WHERE `taypa`=' . $this->db->safe($group) . ' ORDER BY `position`, `id` ASC');
		$response = [
			0 => [],
			1 => [],
			2 => [],
			3 => [],
			4 => [],
			5 => []
		];
		$res = [];
		while($row = $list->fetch_object('DbLesson')){
			array_push($response[$row->getDay() - 1], $this->parser($row));
			array_push($res, [
				'id' => $row->getId(),
				'title' => $row->getTitle(),
				'rooms' => _empty($row->getRooms()) ? [] : $row->getRooms(),
				'building' => $row->getBuilding(),
				'time_start' => $row->getTimeStart(),
				'time_end' => $row->getTimeEnd(),
				'day' => $row->getDay()
			]);
		}
		if ($forApp) return $this->response($res, 200);
		else return $this->response($response, 200);
	}

	private function parser($row){
		return [
			'id' => $row->getId(),
			'group' => $row->getGroup(),
			'title' => $row->getTitle(),
			'rooms' => _empty($row->getRooms()) ? [] : $row->getRooms(),
			'building' => $row->getBuilding(),
			'time' => [
				'start' => $row->getTimeStart(),
				'end' => $row->getTimeEnd()
			],
			'day' => $row->getDay()
		];
	}
}