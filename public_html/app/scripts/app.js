'use strict';

var app = angular
  .module('oshsutimesApp', [
    'ui.bootstrap',
    'ngStorage',
    'ngAlertify',
    'ui.router',
    'ui.router.state.events'
  ])
  .run(function($rootScope, $localStorage, $state, StorageService){
    $localStorage.$default({
      faculty: {},
      group: {},
      lessons: [],
      admin: false,
      password: ''
    });

    $rootScope.dayNames = [
      'Дүйшөмбү / Понедельник',
      'Шейшемби / Вторник',
      'Шаршемби / Среда',
      'Бейшемби / Четверг',
      'Жума / Пятница',
      'Ишемби / Суббота'
    ];

    $rootScope.page = {
      loading: false,
      error: false
    };

    $rootScope.$on('adminChange', function(event){
      $rootScope.admin = StorageService.getAdmin();
    });

    $rootScope.admin = StorageService.getAdmin();

    $rootScope.$on('authorized', function(event){
      $rootScope.faculty = StorageService.getFaculty();
      $rootScope.group = StorageService.getGroup();
      $rootScope.lessons = StorageService.getLessons();
      $rootScope.authorized = true;
    });

    $rootScope.$on('not_authorized', function(event){
      $rootScope.faculty = {};
      $rootScope.group = {};
      $rootScope.lessons = [];
      $rootScope.authorized = false;
      StorageService.destroy();
    });

    if (angular.equals(StorageService.getFaculty(), {}) && angular.equals(StorageService.getGroup(), {})) {
      $rootScope.$broadcast('not_authorized');
      $rootScope.$emit('not_authorized');
    } else {
      $rootScope.$broadcast('authorized');
      $rootScope.$emit('authorized');
    }

    $rootScope.$on("$stateChangeStart", function(event){
      $rootScope.page.loading = true;
    });

    $rootScope.$on("$stateChangeSuccess", function(event){
      $rootScope.page.loading = false;
    });

    $rootScope.$on("$stateChangeError", function(event){
      $rootScope.page.loading = false;
      $rootScope.page.error = true;
    });
  });