'use strict';

app.service('RequestService', function($http, $rootScope, $q, StorageService, alertify){
  return {
	  post: function (controller, method, params) {
	    var result = $q.defer();
	    var data = {};

	    data['secret'] = '6LdH-hATAAAAAPGGD7RwpiOxQthjIsTUgI5cGCn1';
	    data['controller'] = controller;
	    data['method'] = method;
     	data['params'] = params;

	    $rootScope.httPromise = $http({
	      url: '/api',
	      method: 'post',
	      data: $.param(data),
	      cache: false,
	      headers: { 'Content-type': 'application/x-www-form-urlencoded' }
	    }).then(function(response){
	      //setTimeout(function(){
	        if (response.status == 200) {
	            result.resolve(response.data);
	        } else {
	        	alertify.error('Белгисиз катачылык!');
	          result.reject(response);
	        }
	      //}, 2000);
	    }, function(reject){
	    	if (reject.status == 403) alertify.error('Бул аракетти аткарууга сизде укук жок!');
	    	else alertify.error('Интернетке туташуудан катачылык!');
	      result.reject(reject);
      });
      return result.promise;
	  }
  };
});