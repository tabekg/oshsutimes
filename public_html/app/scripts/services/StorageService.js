'use strict';

app.service('StorageService', function($localStorage, $rootScope){
    this.destroy = function(){
	  	$localStorage.faculty = {};
			$localStorage.group = {};
	  	$localStorage.lessons = [];
	  	$localStorage.admin = false;
	  	$localStorage.password = '';
	  	$rootScope.$broadcast('adminChange');
      $rootScope.$emit('adminChange');
	  };
	  this.getFaculty = function(){
	    return $localStorage.faculty;
	  };
	  this.getGroup = function(){
	    return $localStorage.group;
	  };
	  this.getLessons = function(){
	    return $localStorage.lessons;
	  };
	  this.setAdmin = function(admin){
	  	$localStorage.admin = admin;
	  };
	  this.setPassword = function(password){
	  	$localStorage.password = password;
	  };
	  this.getAdmin = function(){
	  	return $localStorage.admin;
	  };
	  this.getPassword = function(){
	  	return $localStorage.password;
	  };
	  this.setLessons = function(lessons){
	  	$localStorage.lessons = lessons;
	  };
	  this.create = function(faculty, group){
	    $localStorage.faculty = faculty;
	    $localStorage.group = group;
	  };
  });