app.config(function($stateProvider, $urlRouterProvider){
  $urlRouterProvider
    .otherwise('/app.index');
  $stateProvider
    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'views/app.html'
    })
    .state('admin', {
      abstract: true,
      url: '/admin',
      templateUrl: 'views/app.html'
    })
    .state('app.index', {
      url: '.index',
      templateUrl: 'views/index.html',
      controller: 'IndexCtrl'
    })
    .state('app.about', {
      url: '.about',
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl'
    })


    .state('admin.login', {
      url: '.login',
      templateUrl: 'views/admin/login.html',
      controller: 'AdminLoginCtrl'
    })
    .state('admin.index', {
      url: '.index',
      templateUrl: 'views/admin/index.html',
      controller: 'AdminIndexCtrl'
    })
    .state('admin.faculties', {
      url: '.faculties',
      templateUrl: 'views/admin/faculties.html',
      controller: 'AdminFacultiesCtrl'
    })
    .state('admin.groups', {
      url: '.groups',
      templateUrl: 'views/admin/groups.html',
      controller: 'AdminGroupsCtrl'
    })
    .state('admin.lessons', {
      url: '.lessons',
      templateUrl: 'views/admin/lessons.html',
      controller: 'AdminLessonsCtrl'
    });
  });