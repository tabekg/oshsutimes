'use strict';

app.controller('AdminFacultiesCtrl', function (StorageService, $state, $scope, RequestService, alertify) {
  if (StorageService.getAdmin()){
  	$scope.faculty = {
      name: '',
      addLoading: '',
      add: function(){
        if (angular.equals($scope.faculty.name, '')){
          alertify.error('Факультет аталышын жазыңыз!');
        } else {
          $scope.faculty.addLoading = true;
          RequestService.post('admin', 'addFaculty', {
            password: StorageService.getPassword(),
            name: $scope.faculty.name
          }).then(function(resolve){
            $scope.faculty.addLoading = false;
            if (resolve.status == 'success'){
              alertify.success('Ийгиликтүү кошулду!');
              $scope.faculty.name = '';
            } else if (resolve.status == 'exists'){
              alertify.error('Бул аталыштагы факультет базада бар!');
            } else {
              alertify.error('Белгисиз катачылык!');
            }
          }, function(reject){
            $scope.faculty.addLoading = false;
            console.error(reject);
          });
        }
      }
    };
  } else {
  	$state.go('admin.login');
  }
});