'use strict';

app.controller('AdminLoginCtrl', function (StorageService, $state, alertify, $scope, RequestService, $rootScope) {
  if (StorageService.getAdmin()){
  	$state.go('admin.index');
  } else {
  	$scope.password = '';
  	$scope.loading = false;
  	$scope.go = function(){
  		if ($scope.loading) return false;
  		if ($scope.password == '') alertify.error('Сыр сөз жазыңыз!');
  		else {
  			$scope.loading = true;
  			RequestService.post('admin', 'login', {
  				password: $scope.password
  			}).then(function(resolve){
		      $scope.loading = false;
		      if (resolve.result == true){
		      	alertify.success('Кош келиңиз!');
		      	StorageService.setPassword(resolve.hash);
		      	StorageService.setAdmin(true);
            $rootScope.$broadcast('adminChange');
            $rootScope.$emit('adminChange');
		      	$state.go('admin.index');
            $rootScope.admin = true;
		      } else {
		      	alertify.error('Сыр сөз туура эмес!');
		      }
		    }, function(reject){
		      console.error(reject);
		      $scope.loading = false;
		      alertify.error('Белгисиз катачылык!');
		    });
  		}
  	}
  }
});