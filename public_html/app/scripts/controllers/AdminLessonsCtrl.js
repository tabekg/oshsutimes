'use strict';

app.controller('AdminLessonsCtrl', function (StorageService, $rootScope, $state, $scope, RequestService, alertify) {
  if (StorageService.getAdmin()){
    $scope.groups = [];
    $scope.loaded = false;
    $scope.lesson = {
      day: 0,
      group: -1,
      time: {
        start: '',
        end: ''
      },
      building: '',
      name: '',
      rooms: '',
      position: ''
    };
    $scope.dayNames = $rootScope.dayNames;
    $scope.loading = false;
    $scope.add = function(){
      if (angular.equals($scope.loading, false)){
        $scope.loading = true;
        RequestService.post('admin', 'addLesson', {
            password: StorageService.getPassword(),
            name: $scope.lesson.name,
            day: $scope.lesson.day,
            time: $scope.lesson.time,
            building: $scope.lesson.building,
            rooms: $scope.lesson.rooms,
            position: $scope.lesson.position,
            group: $scope.lesson.group
        }).then(function(resolve){
            $scope.loading = false;
            if (resolve.status == 'success'){
              alertify.success('Ийгиликтүү кошулду!');
            } else if (resolve.status == 'exists'){
              alertify.error('Базада мындай сабак бар!');
            } else {
              alertify.error('Белгисиз катачылык!');
            }
        }, function(reject){
            $scope.loading = false;
            console.error(reject);
        });
      }
    }

    RequestService.post('group', 'getAll', {}).then(function(resolve){
      $scope.groups = resolve;
      $scope.loaded = true;
    }, function(reject){
      console.error(reject);
      $scope.loaded = false;
    });
  } else {
  	$state.go('admin.login');
  }
});