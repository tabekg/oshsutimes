'use strict';

app.controller('AdminGroupsCtrl', function (StorageService, $state, $scope, RequestService, alertify) {
  if (StorageService.getAdmin()){
    $scope.faculties = [];

  	$scope.group = {
      name: '',
      addLoading: '',
      loaded: false,
      faculty: -1,
      starosta: {
        name: '',
        phone: ''
      },
      add: function(){
        if (angular.equals($scope.group.faculty, -1) || angular.equals($scope.group.name, '') || angular.equals($scope.group.starosta.name, '') || angular.equals($scope.group.starosta.phone, '')){
          alertify.error('Жалпы талаачаларды толтуруңуз!');
        } else {
          $scope.group.addLoading = true;
          RequestService.post('admin', 'addGroup', {
            password: StorageService.getPassword(),
            name: $scope.group.name,
            starosta: $scope.group.starosta,
            faculty: $scope.group.faculty
          }).then(function(resolve){
            $scope.group.addLoading = false;
            if (resolve.status == 'success'){
              alertify.success('Ийгиликтүү кошулду!');
              $scope.group.name = '';
              $scope.group.starosta.name = '';
              $scope.group.starosta.phone = '';
              $scope.group.faculty = -1;
            } else if (resolve.status == 'exists'){
              alertify.error('Базада мындай тайпа бар!');
            } else {
              alertify.error('Белгисиз катачылык!');
            }
          }, function(reject){
            $scope.group.addLoading = false;
            console.error(reject);
          });
        }
      }
    };

    RequestService.post('faculty', 'getAll', {}).then(function(resolve){
      $scope.group.loaded = true;
      $scope.faculties = resolve;
    }, function(reject){
      $scope.group.loaded = false;
      console.error(reject);
    });
  } else {
  	$state.go('admin.login');
  }
});