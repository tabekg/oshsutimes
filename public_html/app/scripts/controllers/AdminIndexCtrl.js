'use strict';

app.controller('AdminIndexCtrl', function (StorageService, $state, $rootScope, $scope, RequestService) {
  if (StorageService.getAdmin()){
  	$scope.logout = function(){
  		StorageService.setAdmin(false);
  		StorageService.setPassword('');
  		$rootScope.admin = false;
      $rootScope.$broadcast('adminChange');
      $rootScope.$emit('adminChange');
  		$state.go('admin.login');
  	}

    $scope.stats = {
      loaded: false,
      breath: 0,
      faculties: 0,
      groups: 0,
      lessons: 0
    };

    RequestService.post('admin', 'getStats', {
      password: StorageService.getPassword()
    }).then(function(resolve){
      $scope.stats = {
        loaded: true,
        breath: resolve.breath,
        faculties: resolve.faculties,
        groups: resolve.groups,
        lessons: resolve.lessons
      };
    }, function(reject){
      $scope.stats = {
        loaded: false,
        breath: 0,
        faculties: 0,
        groups: 0,
        lessons: 0
      };
      console.error(reject);
    });
  } else {
  	$state.go('admin.login');
  }
});