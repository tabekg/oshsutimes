'use strict';

app.controller('IndexCtrl', function(StorageService, RequestService, $scope, $rootScope, $interval, $filter){
  $scope.type = '';

  $scope.faculty = {
      loaded: false,
      model: -1,
      list: []
  };

  $scope.group = {
      loaded: false,
      faculty: 0,
      model: -1,
      list: []
  };

  $scope.button = false;

  $scope.dayNames = [
      'Дүйшөмбү / Понедельник',
      'Шейшемби / Вторник',
      'Шаршемби / Среда',
      'Бейшемби / Четверг',
      'Жума / Пятница',
      'Ишемби / Суббота'
  ];

  $scope.$on('typeWelcome', function(){
    $scope.type = 'welcome';
    $scope.welcomeLoading = true;

    RequestService.post('faculty', 'getAll', {}).then(function(resolve){
      $scope.faculty.list = resolve;
      $scope.faculty.loaded = true;
      $scope.faculty.model = -1;
      $scope.welcomeLoading = false;
    }, function(reject){
      console.error(reject);
      $scope.welcomeLoading = false;
    });

    $scope.reset = function(model){
      $scope.faculty.model = -1;
      $scope.button = false;
      $scope.group = {
        loaded: false,
        faculty: 0,
        model: -1,
        list: []
      };
      $scope.welcomeLoading = false;
    }

    $scope.requestGroup = function(id){
      if (angular.equals(id, -1)){
        $scope.reset();
      } else {
        $scope.button = false;
        $scope.group = {
          loaded: false,
          faculty: 0,
          model: -1,
          list: []
        };
        $scope.welcomeLoading = true;
        RequestService.post('group', 'getByFaculty', {
          faculty: $scope.faculty.list[$scope.faculty.model].id
        }).then(function(resolve){
          $scope.group.list = resolve;
          $scope.group.loaded = true;
          $scope.group.model = -1;
          $scope.welcomeLoading = false;
          $scope.group.faculty = $scope.faculty.model;
        }, function(reject){
          console.error(reject);
          $scope.welcomeLoading = false;
        });
      }
    }

    $scope.requestSubmit = function(group){
      $scope.button = !angular.equals(group, -1);
    }

    $scope.go = function(){
      if ($scope.button){
        StorageService.create(
          $scope.faculty.list[$scope.faculty.model],
          $scope.group.list[$scope.group.model]
        );
        $rootScope.$broadcast('authorized');
        $scope.welcomeLoading = false;
      } return false;
    }
  });

  $scope.$on('typeIndex', function(){
    $scope.type = 'index';

    $scope.schedule = {
      status: [false, false, false, false, false, false]
    };

    $scope.days = StorageService.getLessons();
    $scope.today = $filter('todayLessons')($scope.days);

    $scope.$on('daysUpdated', function(event, data){
      $scope.days = data;
      $scope.today = $filter('todayLessons')($scope.days);
    });

    $scope.min = function(m){
      if (m < 10) return '0' + m;
      else return m;
    }

    $scope.renderTodayText = function(end, nowTime){
      var mTime = (end - nowTime);
      if (mTime < 60){
        return '00:' + $scope.min(mTime);
      } else if (Math.floor((mTime / 60) / 60) > 0){
        return $scope.min(Math.floor(mTime / 60 / 60)) + ':' + ($scope.min(Math.floor(mTime / 60) % 60)) + ':' + $scope.min(mTime % 60);
      } else if (Math.floor(mTime / 60) > 0){
        return $scope.min(Math.floor(mTime / 60)) + ':' + $scope.min(mTime % 60);
      }
    }

    $scope.intervalFunction = function(){
      $scope.todayParams = {
        style: {},
        text: '',
        minitext: 'Сабак жок :)',
        list: []
      };

      $scope.today = $filter('todayLessons')($scope.days);
      var nowTime = new Date();
      nowTime = nowTime.getSeconds() + (nowTime.getMinutes() * 60) + (nowTime.getHours() * 60 * 60);

      if ($scope.today.length > 0){
        angular.forEach($scope.today, function(value, key){
          if (value.time.start <= nowTime && value.time.end >= nowTime) {
            $scope.todayParams.style = angular.equals(value.title, '') ? { 'background-color': 'rgba(0, 255, 0, 0.5)' } : { 'background-color': 'rgba(255, 0, 0, 0.5)' };
            $scope.todayParams.list = [];
            $scope.todayParams.text = $scope.renderTodayText(value.time.end, nowTime);
            $scope.todayParams.minitext = angular.equals(value.title, '') ? 'Дем алуу' : value.title;
          } else if (value.time.start >= nowTime && key == 0){
            $scope.todayParams.style = { 'background-color': 'rgba(0, 255, 0, 0.5)' };
            $scope.todayParams.list = [];
            $scope.todayParams.text = $scope.renderTodayText(value.time.start, nowTime);
            $scope.todayParams.minitext = value.title;
          } else if (value.time.start > nowTime){
            $scope.todayParams.list.push(value);
          }
        });
        if ($scope.todayParams.minitext == 'Сабак жок :)'){
          var lastLesson = $filter('lastLesson')($scope.today);
          if ((parseInt(lastLesson.time.end) + 3600) < nowTime) $scope.todayParams.minitext = 'Сабак бүткөн...';
          else $scope.todayParams.minitext = 'Сабак бүттү :)';
        }
      } else {
        $scope.todayParams.minitext = 'Бүгүн сабак жок :)';
      }
    }

    $scope.intervalFunction();

    $interval(function(){
      $scope.intervalFunction();
    }, 1000);

    $scope.$watch('days', function(){
      angular.forEach($scope.days, function(value, key){
        angular.forEach(value, function(v, k){
          var date = new Date(new Date().setHours(0, 0, 0, 0) + (v.time.start * 1000));
          v.time.startText = date.getHours() + ':' + (date.getMinutes() > 10 ? '' : '0') + date.getMinutes();
          date = new Date(new Date().setHours(0, 0, 0, 0) + (v.time.end * 1000));
          v.time.duration = (v.time.end - v.time.start) / 60;
          v.time.endText = date.getHours() + ':' + (date.getMinutes() > 10 ? '' : '0') + date.getMinutes();
        });
      });
    });

    $scope.requestLessons = function(){
      RequestService.post('lesson', 'getByGroup', {
        group: StorageService.getGroup().id
      }).then(function(resolve){
        StorageService.setLessons(resolve);
        $scope.$broadcast('daysUpdated', resolve);
      }, function(reject){

      });
    }

    $scope.requestLessons();
  });

  $rootScope.$watch('authorized', function(newVal){
    if (newVal) $scope.$emit('typeIndex');
    else $scope.$emit('typeWelcome');
  });
});