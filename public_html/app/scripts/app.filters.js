app
	.filter('lastLesson', function(){
		return function(items){
			var filtered = null;
			for (var i = 0; i < items.length; i++){
				var item = items[i];
				if (filtered == null) filtered = item;
				else {
					if (item.time.start > filtered.time.start) filtered = item;
				}
			}
			return filtered;
		};
	})
	.filter('todayLessons', function(){
		return function(items){
			var week = new Date().getDay();
   		week = week == 0 ? 6 : week - 1;
			return items[week] == undefined ? [] : items[week];
		};
	});