var gulp = require('gulp'),
    uglifycss = require('gulp-uglifycss'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    concatCss = require('gulp-concat-css'),
    rename = require('gulp-rename'),
    ngAnnotate = require('gulp-ng-annotate'),
    stripCssComments = require('gulp-strip-css-comments');

gulp.task('default', [
  'scripts',
  'scripts_vendor',
  'scripts_controllers',
  'scripts_services',
  'styles',
  'styles_vendor'
]);

gulp.task('watch', function(){
  gulp.watch('public_html/app/scripts/*.js', ['scripts']);
  gulp.watch('public_html/app/scripts/controllers/*.js', ['scripts_controllers']);
  gulp.watch('public_html/app/scripts/services/*.js', ['scripts_services']);
  gulp.watch(['public_html/app/styles/**/*.css', 'public_html/app/styles/**/*.sass'], ['styles']);
});

gulp.task('scripts', function(cb){
  pump([
    gulp.src([
      'public_html/app/scripts/app.js',
      'public_html/app/scripts/app.*.js'
    ]),
    ngAnnotate(),
    uglify(),
    concat('app.min.js'),
    gulp.dest('public_html/scripts')
  ], cb);
});

gulp.task('scripts_vendor', function(cb){
  pump([
    gulp.src([
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/angular/angular.min.js',
      'bower_components/bootstrap/dist/js/bootstrap.min.js',
      'bower_components/angular-bootstrap/ui-bootstrap.min.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
      'bower_components/alertifyjs/dist/js/ngAlertify.js',
      'bower_components/angular-ui-router/release/angular-ui-router.min.js',
      'bower_components/angular-ui-router/release/stateEvents.min.js',
      'bower_components/ngstorage/ngStorage.min.js'
    ]),
    ngAnnotate(),
    uglify(),
    concat('vendor.min.js'),
    gulp.dest('public_html/scripts')
  ], cb);
});

gulp.task('scripts_controllers', function(cb){
  pump([
    gulp.src([
      'public_html/app/scripts/controllers/*.js'
    ]),
    ngAnnotate(),
    uglify(),
    concat('controllers.min.js'),
    gulp.dest('public_html/scripts')
  ], cb);
});

gulp.task('scripts_services', function(cb){
  pump([
    gulp.src([
      'public_html/app/scripts/services/*.js'
    ]),
    ngAnnotate(),
    uglify(),
    concat('services.min.js'),
    gulp.dest('public_html/scripts')
  ], cb);
});

gulp.task('styles', function(){
  return gulp.src([
    'public_html/app/styles/**/*.css',
    'public_html/app/styles/**/*.sass'
  ])
    .pipe(sass({
      includePaths: require('node-normalize-scss').includePaths
    }).on('error', sass.logError))
    .pipe(concatCss('app.min.css'))
    .pipe(uglifycss({ 'uglyComments': true }))
    .pipe(stripCssComments())
    .pipe(gulp.dest('public_html/styles'));
});

gulp.task('styles_vendor', function(){
  return gulp.src([
    'bower_components/bootstrap/dist/css/bootstrap.min.css',
    'bower_components/alertifyjs/dist/css/alertify.css',
    'bower_components/fontawesome/css/font-awesome.min.css'
  ])
    .pipe(concatCss('vendor.min.css'))
    .pipe(uglifycss({ 'uglyComments': true }))
    .pipe(stripCssComments())
    .pipe(gulp.dest('public_html/styles'));
});