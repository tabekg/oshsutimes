-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 02, 2018 at 05:48 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_time`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabekg_bi`
--

CREATE TABLE `tabekg_bi` (
  `id` int(12) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabekg_bi`
--

INSERT INTO `tabekg_bi` (`id`, `name`, `value`) VALUES
(1, 'hash', '98431462c3c1a12dfd7deeac412d78d2');

-- --------------------------------------------------------

--
-- Table structure for table `tabekg_faculties`
--

CREATE TABLE `tabekg_faculties` (
  `id` int(12) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabekg_faculties`
--

INSERT INTO `tabekg_faculties` (`id`, `name`) VALUES
(1, 'ФМИТ');

-- --------------------------------------------------------

--
-- Table structure for table `tabekg_groups`
--

CREATE TABLE `tabekg_groups` (
  `id` int(12) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `faculty` int(12) NOT NULL,
  `starosta` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabekg_groups`
--

INSERT INTO `tabekg_groups` (`id`, `title`, `faculty`, `starosta`) VALUES
(1, 'ПОВТАС(б)-2-17Р', 1, ''),
(7, 'ИСТ(б)-2-17', 1, '{"name":"\\u0411\\u0435\\u043b\\u0433\\u0438\\u0441\\u0438\\u0437","phone":"\\u0411\\u0435\\u043b\\u0433\\u0438\\u0441\\u0438\\u0437"}');

-- --------------------------------------------------------

--
-- Table structure for table `tabekg_lessons`
--

CREATE TABLE `tabekg_lessons` (
  `id` int(12) NOT NULL,
  `taypa` int(12) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rooms` longtext COLLATE utf8_unicode_ci NOT NULL,
  `building` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_start` int(35) NOT NULL,
  `time_end` int(35) NOT NULL,
  `day` int(5) NOT NULL,
  `position` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabekg_lessons`
--

INSERT INTO `tabekg_lessons` (`id`, `taypa`, `title`, `rooms`, `building`, `time_start`, `time_end`, `day`, `position`) VALUES
(1, 1, 'Кыргыз тили', '[216]', 'Ош МУ - башкы корпус', 28800, 34500, 1, 1),
(2, 1, 'ЭВМ', '[305]', 'Ош МУ - башкы корпус', 35100, 40800, 1, 3),
(3, 1, 'Программирование', '[328]', 'Ош МУ - башкы корпус', 42600, 48300, 1, 5),
(4, 1, '', '', '', 34500, 35100, 1, 2),
(5, 1, '', '', '', 40800, 42600, 1, 4),
(6, 1, 'Программирование', '[302]', 'Ош МУ - башкы корпус', 28800, 34500, 2, 1),
(7, 1, 'Математика', '[201]', 'Ош МУ - башкы корпус', 35100, 40800, 2, 3),
(8, 1, 'Дене тарбия', '[]', 'Фил.Фак', 42600, 48300, 2, 5),
(9, 1, '', '', '', 34500, 35100, 2, 2),
(10, 1, '', '', '', 40800, 42600, 2, 4),
(11, 1, 'Орус тили', '[320]', 'Ош МУ - башкы корпус', 28800, 34500, 3, 1),
(12, 1, 'Информатика', '[312]', 'Ош МУ - башкы корпус', 35100, 40800, 3, 3),
(13, 1, 'Англис тили', '[318]', 'Ош МУ - башкы корпус', 42600, 48300, 3, 5),
(14, 1, '', '', '', 34500, 35100, 3, 2),
(15, 1, '', '', '', 40800, 42600, 3, 4),
(16, 1, 'ЭВМ', '[303]', 'Ош МУ - башкы корпус', 35100, 40800, 4, 3),
(17, 1, 'Дене тарбия', '[]', 'Фил.Фак', 42600, 48300, 4, 5),
(18, 1, '', '', '', 40800, 42600, 4, 4),
(19, 1, 'Кыргыз тили', '[216]', 'Ош МУ - башкы корпус', 28800, 34500, 5, 1),
(20, 1, 'Англис тили', '[318]', 'Ош МУ - башкы корпус', 35100, 40800, 5, 3),
(21, 1, 'Информатика', '[328]', 'Ош МУ - башкы корпус', 42600, 48300, 5, 5),
(22, 1, '', '', '', 34500, 35100, 5, 2),
(23, 1, '', '', '', 40800, 42600, 5, 4),
(24, 1, 'Орус тили', '[223]', 'Ош МУ - башкы корпус', 35100, 40800, 6, 3),
(25, 1, 'Математика', '[331]', 'Ош МУ - башкы корпус', 42600, 48300, 6, 5),
(26, 1, '', '', '', 40800, 42600, 6, 4),
(201, 7, 'КПВ', '["305а","305б"]', 'Ош МУ - башкы корпус', 28800, 34500, 1, 1),
(202, 7, '', '[]', '', 34500, 35100, 1, 2),
(203, 7, 'КПВ', '["302"]', 'Ош МУ - башкы корпус', 35100, 40800, 1, 3),
(204, 7, 'Дене тарбия', '[]', 'Фил.Фак', 42600, 48300, 1, 5),
(205, 7, 'Кыргыз тили', '["216"]', 'Ош МУ - башкы корпус', 48900, 54600, 1, 7),
(206, 7, '', '[]', '', 40800, 42600, 1, 4),
(207, 7, '', '[]', '', 48300, 48900, 1, 6),
(208, 7, 'Орус тили', '["223","320"]', 'Ош МУ - башкы корпус', 28800, 34500, 2, 1),
(209, 7, 'Математика', '["201"]', 'Ош МУ - башкы корпус', 35100, 40800, 2, 3),
(210, 7, 'Кыргыз тили', '["216"]', 'Ош МУ - башкы корпус', 42600, 48300, 2, 5),
(211, 7, '', '[]', '', 34500, 35100, 2, 2),
(212, 7, '', '[]', '', 40800, 42600, 2, 4),
(213, 7, '', '[]', '', 34500, 35100, 3, 2),
(214, 7, '', '[]', '', 40800, 42600, 3, 4),
(215, 7, 'Дене тарбия', '[]', 'Фил.Фак', 28800, 34500, 3, 1),
(216, 7, 'Кыргыз тили', '["216"]', 'Ош МУ - башкы корпус', 35100, 40800, 3, 3),
(217, 7, 'Орус тили', '["223","320"]', 'Ош МУ - башкы корпус', 42600, 48300, 3, 5),
(218, 7, 'Англис тили', '["318"]', 'Ош МУ - башкы корпус', 28800, 34500, 4, 1),
(219, 7, 'Математика', '["324"]', 'Ош МУ - башкы корпус', 35100, 40800, 4, 3),
(220, 7, 'Англис тили', '["318"]', 'Ош МУ - башкы корпус', 42600, 48300, 4, 5),
(221, 7, '', '[]', '', 34500, 35100, 4, 2),
(222, 7, '', '[]', '', 40800, 42600, 4, 4),
(223, 7, '', '[]', '', 40800, 42600, 5, 4),
(224, 7, '', '[]', '', 34500, 35100, 5, 2),
(230, 7, 'Информатика', '["317"]', 'Ош МУ -  башкы корпус', 35100, 40800, 5, 3),
(226, 7, '', '[]', '', 34500, 35100, 6, 2),
(227, 7, 'Программирование', '["323"]', 'Ош МУ -  башкы корпус', 28800, 34500, 5, 1),
(228, 7, 'Информатика', '["303"]', 'Ош МУ -  башкы корпус', 28800, 34500, 6, 1),
(229, 7, 'Программирование', '["316","314"]', 'Ош МУ -  башкы корпус', 35100, 40800, 6, 3),
(231, 7, 'Англис тили', '["318"]', 'Ош МУ -  башкы корпус', 42600, 48300, 5, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabekg_bi`
--
ALTER TABLE `tabekg_bi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabekg_faculties`
--
ALTER TABLE `tabekg_faculties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabekg_groups`
--
ALTER TABLE `tabekg_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabekg_lessons`
--
ALTER TABLE `tabekg_lessons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabekg_bi`
--
ALTER TABLE `tabekg_bi`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tabekg_faculties`
--
ALTER TABLE `tabekg_faculties`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tabekg_groups`
--
ALTER TABLE `tabekg_groups`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tabekg_lessons`
--
ALTER TABLE `tabekg_lessons`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
